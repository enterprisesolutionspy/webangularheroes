# SPA

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.1.

## INFO 

```
Página web sobre Heroes

- DISPLAY DE HEROES EN TARJETAS
- DISPLAY DE INFORMACION PERSONAL
- CONSUMO Y MUESTRA DE DATOS MEDIANTE PETICIONES
- BUSCADOR INTEGRADO

Desarrollado con Angular 10
```

## REQUIRED
```
NPM
NODEJS
```

## CAPTURAS

![consola-comandos](https://i.ibb.co/wrqrWN5/home.png "Home")
![base-de-datos](https://i.ibb.co/JtNdCCy/heroes.png "Heroes")
![home-page](https://i.ibb.co/SsqqT7m/info-heroes.png "Info Heroes")
![productos](https://i.ibb.co/QHWkBRW/search.png "Buscador")
