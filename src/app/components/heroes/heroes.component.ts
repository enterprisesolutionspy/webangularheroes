import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from "@angular/router";



// Servicio de heroes
import { HeroesService } from "../../services/heroes.service";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})

export class HeroesComponent implements OnInit {

  public heroes:any[] = [];

  constructor( private _heroesService: HeroesService,
                private _router: Router ) 
  { 

  }

  ngOnInit(): void {
    this.heroes = this._heroesService.getHeroes();
  }

  verHeroe(id:number){
    this._router.navigate( ['/heroe',id] );
  }

}
